/* - Link the messaging icon in the navbar to the message page. - U2 */
function linkMessagingIcon(){
	let button;
	if(document.getElementById("fbRequestsJewel")){
		document.getElementById("fbRequestsJewel").parentElement.parentElement.querySelectorAll("a[rel='toggle'][role='button']").forEach(
			function(element){
				let currentCss = window.getComputedStyle(element.firstChild);

				if(currentCss.backgroundPosition == "0px -813px" || currentCss.backgroundPosition == "0px -838px"){
					button = element.parentElement;
				}
			}
		)
	}
	button = button || document.getElementsByName("mercurymessages")[0].parentElement; // fallback
	button.addEventListener("click", function(){window.location = "https://www.facebook.com/messages/"});
	button.querySelector("div[role='dialog']").remove();
}
window.addEventListener("load", linkMessagingIcon);
