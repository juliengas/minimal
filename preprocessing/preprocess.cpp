#include "readFunctions.h"
#include <stdio.h>


int main(int argc, char** argv) {
	bool release = false;
	
	if(argc > 1){
		if(std::string(argv[1]).compare("release") == 0){
			release = true;
		}
	}
	
  std::map<std::string, std::string> manifesto = readManifesto("./MANIFESTO.md");
  std::string version =  readVersion("./data/version");
  std::vector<Website> websites = readSupportedWebsites("./data/supportedWebsites.csv");

  // Create background and content scripts elements and url list
  std::string backgroundScripts;
  std::stringstream elements;
  std::stringstream urlList;
  for (auto website: websites) {
    backgroundScripts += website.printBackgroundScripts();
    elements << website.printElement();
    urlList << website.printUrls();
    if (website.getName() != websites.back().getName()) {
      elements << "," << std::endl;
      urlList << ", ";
    }
  }
  
  // Create manifest.json
  std::ofstream manifestFile;
  
  std::string manifestPath;
  if(release){
  	manifestPath = "./manifest.json";
  } else {
  	manifestPath = "./manifest_dev.json";
  }
  manifestFile.open(manifestPath);
  
  
  std::string manifestTemplatePath;
  if(release){
  	manifestTemplatePath = "./data/manifestTemplate.txt";
  } else {
  	manifestTemplatePath = "./data/manifestTemplate_dev.txt";
  }
  std::ifstream manifestTemplate(manifestTemplatePath);
  std::string manifestLine;
  while (std::getline(manifestTemplate, manifestLine)) {
    replace(manifestLine, "<version>", version);
    replace(manifestLine, "<background_scripts>", backgroundScripts.substr(0, backgroundScripts.length() - 2));
    replace(manifestLine, "<content_scripts>", elements.str());
    replace(manifestLine, "<permissions_urls>", urlList.str());
    manifestFile << manifestLine << std::endl;
  }
  manifestFile.close();

  // Update minimal avatar
  if(release){
    std::ofstream minimalAvatarFile("./pictures/logo/minimalAvatar.svg");
    std::ifstream minimalAvatarTemplate("./data/minimalAvatarTemplate.svg");
    std::string minimalAvatarLine;
    while (std::getline(minimalAvatarTemplate, minimalAvatarLine)) {
      replace(minimalAvatarLine, "[V]", version);
      minimalAvatarFile << minimalAvatarLine << std::endl;
    }
    minimalAvatarFile.close();
  }
  
  // Create CHANGES.md
  
  std::string changesFilePath;
  if(release){
  	changesFilePath = "./CHANGES.md";
  } else {
  	changesFilePath = "./CHANGES_dev.md";
  }
  
  std::ofstream changesFile;
  changesFile.open(changesFilePath);
  
  
  changesFile << "# Changes" << std::endl << "Here is the list of the changes to the internet experience enabled by the last release of minimal (" << version << "). All changes are made client-side by the user and have no effects on the websites themselves." << std::endl << std::endl;
  
  for (auto website: websites) {
    std::string websiteName = website.getName();
    changesFile << "## " << capitalize(websiteName) << std::endl;
    changesFile << website.printComments(manifesto, version) << std::endl;
  }
  
  changesFile.close();
  
  return 0;
}
